db.products.insertMany([
    {
        name: "Iphone X",
        price: 30000,
        isActive: true
    },
    {
        name: "Samsung Galaxy S21",
        price: 51000,
        isActive: true
    },
    {
        name: "Razer Blackshark V2X",
        price: 2800,
        isActive: false
    },
    {
        name: "RAKK Gaming Mouse",
        price: 1800,
        isActive: true
    },
    {
        name: "Razer Mechanical Keyboard",
        price: 4000,
        isActive: true
    },
])

db.getCollection('products').find({})    
    
// Query Operators
// Query Operators allow for more flexible in MongoDB.
// Instead of just having to find/search for documents with exact and definitive values.
// We could use query operators to define conditions instead of just specific criteriaa and value.
// $gt. $lt. $gte, $lte
    
    // $gt - greater than query operator
        // find all items in the products collection whose price is greater than 3000
    db.products.find({price:{$gt:3000}})
    db.products.find({price:{$gt:10000}})
    
    // $lt - less than query operator
        // find all items in the products collection whose price is less than 4000
    db.products.find({price:{$lt:4000}})
    db.products.find({price:{$lt:10000}})
    
    // $gte - greater than or equal
    db.products.find({price:{$gte:4000}})
    db.products.find({price:{$gte:2800}})
    
    // $lte - less than or equal operator
    db.products.find({price:{$lte:4000}})
    db.products.find({price:{$lte:30000}})
    
    // Query operators can also be used to expand queries when deleting or updating.
    db.products.updateMany({price:{$gte:30000}},{$set:{isActive:false}})
    
    
// Mini Activity
db.users.insertMany([
    {
        firstName: "Mary Jane",
        lastName: "Watson",
        email: "ajtiger@mail.com",
        password: "tigerjackpot15",
        isAdmin: false                  
    },
    {
        firstName: "Gwen",
        lastName: "Stacy",
        email: "stacyTech@mail.com",
        password: "stacyTech1991",
        isAdmin: true                  
    },
    {
        firstName: "Peter",
        lastName: "Parker",
        email: "peterWebDev@mail.com",
        password: "webDeveloperPeter",
        isAdmin: true                  
    },
    {
        firstName: "Jonah",
        lastName: "Jameson",
        email: "jjjameson@mail.com",
        password: "spideyisamenace",
        isAdmin: false                  
    },
    {
        firstName: "Otto",
        lastName: "Octavius",
        email: "ottoOctopi@mail.com",
        password: "docOck15",
        isAdmin: true                  
    },
])
    
db.users.find()
    
    
// $regex - this query operator will allow us to match/find documents which will match the pattern/characters that we are looking for.
    
    db.users.find({firstName:{$regex:'O'}})
    db.users.find({firstName:{$regex:'o'}})
    
    // $options - used to make our $regex non-case sensitive.
    db.users.find({firstName:{$regex:'O',$options:'$i'}})
    
    // We can also use $regex to find document which matches a specific pattern or word in a field.
    db.users.find({email:{$regex:'web',$options:'$i'}})
    db.products.find({name:{$regex:'phone',$options:'$i'}})
    








// Mini-Activity
    db.products.find({name:{$regex:'razer',$options:'$i'}})
    db.products.find({name:{$regex:'rakk',$options:'$i'}})
    







// $or and $and
    // $or operator - || - allows us to have a logical operation wherein we can look or find for documents which can satisfy at least one of our conditions.
    
    db.products.find({$or:[{name:{$regex:'x',$options:'$i'}},{price:{$lte:10000}}]})
    db.products.find({$or:[{name:{$regex:'x',$options:'$i'}},{price:30000}]})
    db.users.find({$or:[{firstName:{$regex:'a', $options:'i'}},{isAdmin:true}]})
    db.users.find({$or:[{firstName:{$regex:'e', $options:'i'}},{isAdmin:true}]})
    
// $and operator - && - allows us to have a logical operation wherein we can look or find for documents which satisfy all conditions
    
    db.products.find({$and:[{name:{$regex:'razer', $options:'$i'}},{price:{$gte:3000}}]})
    db.users.find({
        $and:[
                {firstName:{$regex:'e', $options:'$i'}},
                {lastName:{$regex:'a', $options:'$i'}}
        ]
    })
    
    db.products.find({
        $and:[
                {price:{$gt:3000}},
                {price:{$lt:50000}}
          ]
    })
    

// Field Projection
    // find() actually can have 2 arguments, the search criteria and the projection.
    // inclusion or exclusion of fields in the returned documents.
    // .find({query},{projection})
    
    db.users.find({},{"_id":0,"password":0})
    // In field projection, we can show/hide fields: 0 for hide and 1 for show.
    db.users.find({},{"_id":0,"password":1})
    // In field projection, we can implicitly hide other fields by only showing or including the needed fiels.
    db.users.find({},{"password":1})
    db.users.find({},{"firstName":1, "lastName":1, "email":1})
    db.users.find({},{"_id":0, "firstName":1, "lastName":1, "email":1})
    
    
    
    
// Mini-Activity:
    // Look for users whose isAdmin property is true but only show their email.
    db.users.find({"isAdmin": true},{"_id":0,"email":1})

    

    

    

// Take-Home-Mini-Activity

    // Find products with letter 'y' in the name AND price less than 10000

        // show only its name and price

    

    //Find products with letter 'mouse' in the name

        // show only its name and price

    

    db.products.find({$and:[{name:{$regex:'y', $options:'$i'}},{price:{$lt:10000}}]},{"_id":0,"name":1,"price":1})

    

    db.products.find({name:{$regex:'mouse',$options:'$i'}},{"_id":0,"name":1,"price":1})